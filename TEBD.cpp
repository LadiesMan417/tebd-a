#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <cassert>
#define ARMA_64BIT_WORD
#include <armadillo>

#include "TEBD.h"

using namespace std;
using namespace arma;

TEBD::TEBD(int truncateChi, int size, int dim)
	:truncateChi(truncateChi), size(size), dim(dim),
     tensorGamma(new cx_cube* [size]),   // 0, 1, ..., size-1
     storeLambda(new cx_vec* [size+1]),  // -1, 0, 1, ..., size-1; where -1 and size-1 is dummy vec
     storeInfoChi(new int [size+1]),
     hamiltonian(dim*dim,dim*dim,size-1)
{
	// infoChi
	for (int i=0; i<size+1; i++){
        int vertualbd; // 2^0, 2^1, ..., 2^(chi/2), ..., 2^0
        if (i <= size/2)
            vertualbd = (float)i <= log2((float)truncateChi) ? pow(dim,i) : truncateChi;
        else
            vertualbd = (float)(size-i) <= log2((float)truncateChi) ? pow(dim,size-i) : truncateChi;
        storeInfoChi[i] = vertualbd;
	}
	infoChi = &storeInfoChi[1]; // allow negative index access

	// tensorGamma
	for (int i=0; i<size; i++){
		tensorGamma[i] = new cx_cube(infoChi[i-1],infoChi[i],dim);
		tensorGamma[i]->zeros();
	}

	// storeLambda and tensorLambda
	for (int i=0; i<size+1; i++){
		storeLambda[i] = new cx_vec(storeInfoChi[i]);
		storeLambda[i]->zeros();
	}
	tensorLambda = &storeLambda[1];
}

TEBD::~TEBD(){
	for (int i=0; i<size; i++){
		delete tensorGamma[i];
		delete storeLambda[i];
	}
	delete storeLambda[size];
	delete [] tensorGamma;
	delete [] storeLambda;
}

void TEBD::InitializeMPS(){
		// Construct tensorGamma [size] and tensorLambda [siz-1]
		arma_rng::set_seed_random();
		for (int i=0; i<size; i++){
			if (i<size-1) (*tensorLambda[i]).set_real(sort(normalise(randg<vec>(infoChi[i])), "descend"));
			for (int j=0; j<dim; j++){
				tensorGamma[i]->slice(j).randu();
			}
		}
		tensorLambda[-1]->ones();
		tensorLambda[size-1]->ones();
}


void TEBD::SetController(double lepsilon, double ltimestep,
                         int ltrotterNumber, double lrefinement){
	epsilon = lepsilon;
	timestep = ltimestep;
	trotterNumber = ltrotterNumber;
	refinement = lrefinement;

	localTimestep = ltimestep/ltrotterNumber;
}

void TEBD::SetHamiltonian(const cx_cube& localHamil){
	assert(localHamil.n_rows == dim*dim && 
           localHamil.n_cols == dim*dim &&
           localHamil.n_slices == size-1);
	hamiltonian = localHamil;
}

void TEBD::Evolve(){
	cout.precision(15);
	cout.setf(ios::fixed);

	double localError = 1.0;
	double localTime = 0.0;
	double lastValue = 0.0;
	double curValue = 0.0;
	while (abs(localError) > epsilon){
//		cout << "delta = " << localTimestep << endl;
		// apply half gate
		for (int site = 0; site < size-1; site+=2)
			TwoSitesUpdate(site, site+1, -1);

		// measuring
		for (int t=0; t<trotterNumber; t++){
			if (t != trotterNumber-1) {
				for (int site = 1; site < size-1; site+=2)
					TwoSitesUpdate(site, site+1, 1);
				for (int site = 0; site < size-1; site+=2)
					TwoSitesUpdate(site, site+1, 1);
			}
			else { // t == trotterNumber-1
				for (int site = 1; site < size-1; site+=2)
					TwoSitesUpdate(site, site+1, 1);
			}

			localTime += localTimestep;
			lastValue = curValue;
			curValue = Expectation(0);
			localError = curValue - lastValue;
		
			// time, ground state energy, energy per site, error
/*
			cout << "T = " << localTime << ", E = " << curValue 
                 << ", E/N = " << curValue/size << ", e = " << localError << endl;
*/
			// if measuring step converge already
			if (abs(localError) < epsilon) {
				break;
			}
//			GetTensorGamma();
//			GetTensorLambda();
		}

		// apply half gate
		for (int site = 0; site < size-1; site+=2)
			TwoSitesUpdate(site, site+1, -1);
		lastValue = curValue;
		curValue = Expectation(0);
		localError = curValue - lastValue;
		cout << "T = " << localTime << ", E = " << curValue 
             << ", E/N = " << curValue/size << ", e = " << localError << endl;

		// refining		
		localTimestep /= refinement;
		trotterNumber *= refinement;
	}
}

// Core operation, application of a two-site gate/operator on a matrix product state
void TEBD::TwoSitesUpdate(int left, int right, int gateType){
	assert(left >= 0 && left < right && right <= size-1);
	
	// Construct tensorTheta
	cx_mat tensorTheta = zeros<cx_mat>(dim*infoChi[left-1],dim*infoChi[right]);
	ConstructTheta(tensorTheta, left, right, gateType);
//	tensorTheta.print("\ntensorTheta:");

	// Construct reduced density matrix
	cx_mat tensorReduced = zeros<cx_mat>(dim*infoChi[right],dim*infoChi[right]);
	cx_mat tmpReduced = zeros<cx_mat>(dim*infoChi[left-1],dim*infoChi[right]);
	for (int alpha=0; alpha<infoChi[left-1]; alpha++){ 
		for (int i=0; i<dim; i++){
			tmpReduced.row(alpha*dim+i) = tensorTheta.row(alpha*dim+i)*(*tensorLambda[left-1])(alpha);
		}
	}
	tensorReduced = (tmpReduced.t()*tmpReduced).st();
//	tensorReduced.print("\n"); // see if is hermitian

	for (int j=0; j<dim; j++){
		for (int gamma=0; gamma<infoChi[right]; gamma++){
			tensorReduced.col(j*infoChi[right]+gamma) = tensorReduced.col(j*infoChi[right]+gamma)*(*tensorLambda[right])(gamma);
			tensorReduced.row(j*infoChi[right]+gamma) = tensorReduced.row(j*infoChi[right]+gamma)*(*tensorLambda[right])(gamma);
//			tensorReduced.print("\n");
		}
	}
//	tensorReduced.print("\ntensorReduced:"); /// see if is hermitian
	
	// Diagonalizing and truncating
	cx_vec eigVal;
	cx_mat eigVec;
	eig_gen(eigVal, eigVec, tensorReduced);
	uvec sortIndices = sort_index(eigVal, "descend");

	/*
	cout << "================eig_gen================" << endl;
	cout << "\nat site = " << left << endl;
	cout.precision(15);
	cout.setf(ios::fixed);
	eigVal.raw_print(cout, "\neigVal:");
	sortIndices.print("\nindices:"); 
	cout << "=======================================" << endl;
    */
	//cout << "site = " << left << ", accu = " << accu(eigVal) << endl;

	tensorLambda[left]->zeros();
	for (int beta=0; beta<infoChi[left]; beta++){ // truncation dim*chi -> chi
		double realEigVal = real(eigVal(sortIndices(beta)));
//		if (abs(realEigVal) < 1.e-15) realEigVal = 0.0;
//		assert( realEigVal >= 0 ); // should be positive quantity
		(*tensorLambda[left])(beta) = sqrt(abs(realEigVal)); //XXX
		for (int j=0; j<dim; j++){
			tensorGamma[right]->slice(j).row(beta) = eigVec.col(sortIndices(beta)).subvec(infoChi[right]*j,infoChi[right]*(j+1)-1).st();
		}	
	}
	// Reconstruct tensorGamma[right] and tensorLambda[left]
/*	
	cout << "================right-normalized condition================" << endl;
	cx_mat tmpCheckRight = zeros<cx_mat>(infoChi[left], infoChi[left]);
	for (int i=0; i<dim; i++){
		tmpCheckRight += (tensorGamma[right]->slice(i))*(tensorGamma[right]->slice(i).t());
	}
	tmpCheckRight.print("\nright normalized check:");
	cout << "==========================================================" << endl;
*/

	for (int j=0; j<dim; j++) 
		tensorGamma[right]->slice(j).each_row() /= tensorLambda[right]->st();


	// Reconstruct tensorGamma[left]
	Permute(tensorTheta, infoChi[left-1], 1);
	for (int i=0; i<dim; i++){
		tensorGamma[left]->slice(i).zeros();
		for (int j=0; j<dim; j++){
			// X.submat( first_row, first_col, last_row, last_col )
			tensorGamma[left]->slice(i) += (tensorTheta.submat(infoChi[left-1]*i,infoChi[right]*j,infoChi[left-1]*(i+1)-1,infoChi[right]*(j+1)-1).each_row()
                                         % square(tensorLambda[right]->st())) * tensorGamma[right]->slice(j).t();
		}
		tensorGamma[left]->slice(i).each_row() /= tensorLambda[left]->st();
	}

/*
	cout << "================left-normalized condition================" << endl;
	cx_mat tmpCheckLeft = zeros<cx_mat>(infoChi[left], infoChi[left]);
	for (int i=0; i<dim; i++){
		cx_mat tmpTensorGammaLeft = tensorGamma[left]->slice(i).each_col() % (*tensorLambda[left-1]);
		tmpCheckLeft += (tmpTensorGammaLeft.t())*(tmpTensorGammaLeft);
	}
	tmpCheckLeft.print("\nleft normalized check:");
	cout << "=========================================================" << endl;
*/

	// Renormalized tensorLambda[left] at last 
	(*tensorLambda[left]) = normalise((*tensorLambda[left])); // due to truncation and nonunitary operation,
}


double TEBD::Expectation(int gateType){
	double value = 0.0;
	for (int site=0; site<size-1; site++){
		cx_mat tensorThetaH = zeros<cx_mat>(dim*infoChi[site-1],dim*infoChi[site+1]); // gateType = hamiltonian
		cx_mat tensorThetaI = zeros<cx_mat>(dim*infoChi[site-1],dim*infoChi[site+1]); // gateType = identity
		ConstructTheta(tensorThetaH, site, site+1, gateType);
		ConstructTheta(tensorThetaI, site, site+1, 2);
		for (int alpha=0; alpha<infoChi[site-1]; alpha++){ 
			for (int i=0; i<dim; i++){
				tensorThetaH.row(alpha*dim+i) = tensorThetaH.row(alpha*dim+i)*(*tensorLambda[site-1])(alpha);
				tensorThetaI.row(alpha*dim+i) = tensorThetaI.row(alpha*dim+i)*(*tensorLambda[site-1])(alpha);
			}
		}
		for (int j=0; j<dim; j++){
			for (int gamma=0; gamma<infoChi[site+1]; gamma++){
				tensorThetaH.col(j*infoChi[site+1]+gamma) = tensorThetaH.col(j*infoChi[site+1]+gamma)*(*tensorLambda[site+1])(gamma);
				tensorThetaI.col(j*infoChi[site+1]+gamma) = tensorThetaI.col(j*infoChi[site+1]+gamma)*(*tensorLambda[site+1])(gamma);
			}
		}

		value += real(accu(conj(tensorThetaI)%tensorThetaH));
	}
	return value;
}

void TEBD::ConstructTheta(cx_mat& tensorTheta, int left, int right, int gateType){
	assert(tensorTheta.n_rows == dim*infoChi[left-1] && tensorTheta.n_cols == dim*infoChi[right]);

	// Contruct tensorA
	cx_cube tensorA = zeros<cx_cube>(infoChi[left-1],infoChi[right],dim*dim);
	for (int k=0; k<dim; k++){
		for (int l=0; l<dim; l++){ // k*dim+l is a rewrite of index, for each kl
		tensorA.slice(k*dim+l) = (tensorGamma[left]->slice(k).each_row() % tensorLambda[left]->t())*tensorGamma[right]->slice(l);
		}
	}
	// Construct tensorV
	cx_cube tensorV = zeros<cx_cube>(dim,dim,dim*dim); // O(d^4) time complexity
	cx_mat tmpHamil = zeros<cx_mat>(dim*dim,dim*dim);
	// gateType
	if (gateType == -1)
		tmpHamil = expmat(-hamiltonian.slice(left)*localTimestep/2.0);
	else if (gateType == 1)
		tmpHamil = expmat(-hamiltonian.slice(left)*localTimestep);
	else if (gateType ==0)
		tmpHamil = hamiltonian.slice(left);
	else if (gateType == 2)
		tmpHamil = eye<cx_mat>(dim*dim,dim*dim);
	else if (gateType == 11)
		tmpHamil = expmat(-cx_double(0.0,1.0)*hamiltonian.slice(left)/2.0*localTimestep);
	else
		assert(gateType == -1 || gateType == 1 || gateType == 0 || gateType == 2 || gateType == 11);
    //cout << localTimestep << endl;

//	(tmpHamil*tmpHamil.t()).print();

	for (int k=0; k<dim; k++){
		for (int l=0; l<dim; l++){
			for (int i=0; i<dim; i++){
				for (int j=0; j<dim; j++){
					tensorV.slice(k*dim+l)(i,j) = tmpHamil(i*dim+j,k*dim+l);
				}
			}
		}
	}

	// Construct tensorTheta
	for (int k=0; k<dim; k++){
		for (int l=0; l<dim; l++){
			cx_mat tmpTheta = kron(tensorV.slice(k*dim+l),tensorA.slice(k*dim+l));
//			tmpTheta.print("\n");
			Permute(tmpTheta, infoChi[left-1], 0);
//			tmpTheta.print("\n"); 
			tensorTheta += tmpTheta;
		}
	}
//	tensorTheta.print("\n");
}

void TEBD::GetTensorGamma() const{
	GetTensorGamma(0, size-1);
}

// Get
void TEBD::GetTensorGamma(int start, int end) const{
	assert(start>=0 && start<=end && end<size);
	for (int i=start; i<=end; i++){
		for (int j=0; j<dim; j++){
			tensorGamma[i]->slice(j).print("\nGamma["+to_string(i)+"]["+to_string(j)+"]"
                                          +" ("+to_string(infoChi[i-1])+"x"+to_string(infoChi[i])+")");
		}
	}
}

void TEBD::GetTensorLambda() const{
	GetTensorLambda(0, size-2);
}

void TEBD::GetTensorLambda(int start, int end) const{
	assert(start>=-1 && start<=end && end<size); // -1 and size-1 are valid
	for (int i=start; i<=end; i++){
		cout.precision(15);
		cout.setf(ios::fixed);
		tensorLambda[i]->raw_print(cout, "\nlambda["+to_string(i)+"]"+" ("+to_string(infoChi[i])+")");
//		cout << "norm = " << norm(*tensorLambda[i],2);
//		cout << (*tensorLambda[i])(0) << " " << (*tensorLambda[i])(1);
	}
}

void TEBD::GetHamiltonian() const{
	GetHamiltonian(0, size-2);
}

void TEBD::GetHamiltonian(int start, int end) const{
	assert(start>=0 && start<=end && end<size-1);
	for (int i=start; i<=end; i++){
		cout << "[" << i << "]";
		hamiltonian.slice(i).print("\n");
	}
}

void TEBD::Permute(cx_mat& tensorP, int localChi, int index){
	typedef map<int, vector<pair<int,int> > > swapMap; //XXX slow?

	swapMap::iterator itm = swapIndices.find(localChi);
	vector<pair<int,int> >::reverse_iterator itvrb;
	vector<pair<int,int> >::reverse_iterator itvre;
	vector<pair<int,int> >::iterator itvb;
	vector<pair<int,int> >::iterator itve;

	if (itm != swapIndices.end()){
		itvrb = itm->second.rbegin();
		itvre = itm->second.rend();
		itvb = itm->second.begin();
		itve = itm->second.end();
	}
	else {
		int tmpVec[dim*localChi];
		for (int i=0; i<dim*localChi; i++){
			tmpVec[i] = i%dim;
		}
	
		pair<swapMap::iterator,bool> ret;
		ret = swapIndices.insert(make_pair(localChi,vector<pair<int,int> >()));
		for (int count=0; count<dim; count++){
			int overlap = 1;
			for (int i=1; i<dim*localChi-1; i++){ // first and last one are in place already
				if (tmpVec[i] > tmpVec[i+1]){
					for (int j=0; j<overlap; j++){
						int tmp = tmpVec[i-j]; // swap in tmpVec
						tmpVec[i-j] = tmpVec[i+1-j];
						tmpVec[i+1-j] = tmp;
						ret.first->second.push_back(make_pair(i-j,i+1-j));
					}
				}
				else if (tmpVec[i] == tmpVec[i+1]){
					overlap++;
				}
				else
					continue;
			}	
		}
		itvrb = ret.first->second.rbegin();
		itvre = ret.first->second.rend();
		itvb = ret.first->second.begin();
		itve = ret.first->second.end();
/*
		cout << "===============swap indices in order===============" << endl;
		for (vector<pair<int,int> >::iterator it=ret.first->second.begin(); it!=ret.first->second.end(); it++){
			cout << "(" << it->first << "," << it->second << ")" << endl;
		}  
		cout << "===================================================" << endl;
*/
	}

	if (index == 0){ // reverse permute
		for (vector<pair<int,int> >::reverse_iterator it=itvrb; it!=itvre; it++){
			tensorP.swap_rows(it->first, it->second);
		}
	}
	else if (index == 1){ // forward permute
		for (vector<pair<int,int> >::iterator it=itvb; it!=itve; it++){
			tensorP.swap_rows(it->first, it->second);
		}
	}
	else 
		assert(index == 0 || index == 1);

}

