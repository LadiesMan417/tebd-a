#ifndef TEBD_H
#define TEBD_H

#define ARMA_64BIT_WORD
#include <armadillo>
#include <vector>
#include <map>
#include <utility>

class TEBD{
private:
	// MPS
	int truncateChi;               // truncated Schmidt rank
	int size;                      // system size
	int dim;                       // spin dimension
	arma::cx_cube** tensorGamma;   // four-dimensional (chi,chi,dim,size) array of gamma matrices
                                   // each site contains different localChi
	arma::cx_vec** storeLambda;    // a set of vectors, (chi,size-1) lambda matrices
                                   // each site contains different localChi
                                   // should not be accessed directly, other than constructor and desturcto/int/int/g
	arma::cx_vec** tensorLambda;   // to access negative index of sotreLambda
	int* storeInfoChi;             // store the information of localChi
	int* infoChi;                  // accessor to sotreInfoChi    

	// Hamiltonian
	arma::cx_cube hamiltonian;     //two-site operator of dimension (dim^2,dim^2, size-1)
	// Controller
	double epsilon;                // accuracy, convergence criteria
	double localTimestep;          // \delta = \tau / trotterNumber
	double timestep;               // \tau or endtime
	int trotterNumber;             // number of iteraction of each value of local timestep
	double refinement;             // refinement of local timestep

	// Miscellaneous 
	int toIndex(int k, int l, int index);                    // convert base dim to base 10
	std::map<int, std::vector<std::pair<int,int> > > swapIndices; // used for function Permute

	// Private functions
	void Permute(arma::cx_mat& tensorP, int localChi, int index);  // used for construction of Theta

public:
	// Initializer & Set
	TEBD(int chi, int size, int dim); // no default constructor
	~TEBD();
	void InitializeMPS();                            // 1:simple matrix state, 2:randomize
	void SetController(double epsilon, double timestep,
                       int trotterNumber, double refinement);
	void SetHamiltonian(const arma::cx_cube&);

	// Core operations
	void Evolve(); 
	void TwoSitesUpdate(int left, int right, int gateType);
	double Expectation(int gateType); // supports only combination of two sites operator, see gateType
	void ConstructTheta(arma::cx_mat& tensorTheta, int left, int right, int gateType);

	// Get
	void GetTensorGamma() const;
	void GetTensorGamma(int start, int end) const; // specify range to output, default all sites
	void GetTensorLambda() const;
	void GetTensorLambda(int start, int end) const;
	void GetHamiltonian() const;
	void GetHamiltonian(int start, int end) const;
};
#endif
