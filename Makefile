INC := $(ARMADILLO_ROOT)/include
LIB := $(ARMADILLO_ROOT)/lib
CC:= g++
DEBUG:= -g -fsanitize=address
FLAG:= -std=c++11 -O2

main.e: main.o TEBD.o
	$(CC) $(FLAG) -I$(INC) -L$(LIB) $^ -o $@ -larmadillo
main.o: main.cpp TEBD.cpp TEBD.h
	$(CC) $(FLAG) -I$(INC) -c $<
TEBD.o: TEBD.cpp TEBD.h
	$(CC) $(FLAG) -I$(INC) -c $<

clean:
	rm *.o *.e
