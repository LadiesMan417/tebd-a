#include <iostream>
#include <string>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <ratio>
#include <chrono>
using namespace std::chrono;
#define ARMA_64BIT_WORD
#include <armadillo>

#include "TEBD.h"

using namespace std;
using namespace arma;


cx_cube ConstructHamiltonian(unsigned, unsigned, string);

int main(){

	unsigned chi = 16;
	unsigned size = 64;
	unsigned dim = 2;
    clock_t start, end;
    start = clock();
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
	
    TEBD Ising(chi, size, dim);
	Ising.InitializeMPS();
//	Ising.GetTensorGamma(18,19);
//	Ising.GetTensorLambda(18,19);

	Ising.SetHamiltonian(ConstructHamiltonian(size, dim, "Ising"));
//	Ising.GetHamiltonian();

	Ising.SetController(1e-8, 10, 1, 2);

//    Ising.TwoSitesUpdate(4, 5, 1);

/*
	for (int i=0; i<100; i++){
		Ising.TwoSitesUpdate(0,1,1);
		Ising.GetTensorGamma();
		Ising.GetTensorLambda();
	}
*/

//	Ising.TwoSitesUpdate(0,1,1);


//	Ising.GetTensorGamma(18,19);	
//	Ising.GetTensorLambda(18,19);

//	double value = Ising.Expectation(0);
//	cout << "value = " << value << endl;

	Ising.Evolve();
    
    end = clock();
    cout << "CPU time used: " << (float)(end-start) / CLOCKS_PER_SEC << " seconds" << endl;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
    cout << "Wall clock time passed: " << time_span.count() << " seconds" << endl;

	return 0;
}

cx_cube ConstructHamiltonian(unsigned size, unsigned dim, string name){

	// Pauli matrices
	cx_mat pauliX = {{cx_double(0.0,0.0),cx_double(1.0,0.0)}
                    ,{cx_double(1.0,0.0),cx_double(0.0,0.0)}};
	cx_mat pauliZ = {{cx_double(1.0,0.0),cx_double(0.0,0.0)}
                    ,{cx_double(0.0,0.0),cx_double(-1.0,0.0)}};
	// Spin operators
	cx_mat spinP = {{cx_double(0.0,0.0),cx_double(1.0,0.0)}
                   ,{cx_double(0.0,0.0),cx_double(0.0,0.0)}};
	cx_mat spinM = {{cx_double(0.0,0.0),cx_double(0.0,0.0)}
                   ,{cx_double(1.0,0.0),cx_double(0.0,0.0)}};

	cx_cube hamil = zeros<cx_cube>(dim*dim,dim*dim,size-1);

	if (name == "Ising"){
		assert(dim == 2); // transverse Ising model with spin half

		cx_double J(1.0,0.0); // coupling constant
		cx_double B(1.0,0.0); // applied field
		cx_mat identity = eye<cx_mat>(dim,dim);

		for (unsigned i=0; i<size-1; i++){
			hamil.slice(i) = -J*kron(pauliZ,pauliZ) - kron(B*pauliX/2.0,identity) -
        	                 kron(identity,B*pauliX/2.0);
		}
		hamil.slice(0) -= kron(B*pauliX/2.0,identity);
		hamil.slice(size-2) -= kron(identity,B*pauliX/2.0);
	}
	else if (name == "Heisenberg"){
		assert(dim == 2); // spin half Heisenberg
	

		for (unsigned i=0; i<size-1; i++){
			hamil.slice(i) = kron(0.5*pauliZ,0.5*pauliZ) + 0.5*kron(spinP,spinM)
                           + 0.5*kron(spinM,spinP);
		}
	}
	else {
		assert(name == "Ising" || name == "Heisenberg");
	}

	return hamil;
}
